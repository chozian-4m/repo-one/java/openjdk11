# Description

RedHat OpenJDK 11 (JRE only). Supported through October 2024. For more information about OpenJDK life cycle and support please click [here](https://access.redhat.com/articles/1299013).

See [openjdk11-devel](https://repo1.dsop.io/dsop/opensource/openjdk/openjdk11-devel) for developer version.

# Environment Variables
This image has the following environment variables which can be used to customize behavior:
- **JAVA_HOME**: /usr/lib/jvm/jre-11-openjdk
